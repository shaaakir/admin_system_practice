<?php
    session_start();

    if (isset($_SESSION['done'])){
        echo $_SESSION['done'];
        unset($_SESSION['done']);
    }

?>
<!DOCTYPE html>
<html lang="en-US" xmlns="http://www.w3.org/1999/html">
	<head>
		<meta charset="UTF-8">
		<title>My Folio (Project 3)</title>
		<link rel="stylesheet" href="css/font-awesome.min.css" />
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
		<div class="full_wrapper full_header">
			<div class="main_wrapper main_header">
				<div class="logo_part floating">
					<a href="#"><img src="img/logo.png" alt="My Folio" /></a>
				</div>
				<div class="nav floating">
					<ul>
						<li><a href="#home">home</a></li>
						<li><a href="#about">about me</a></li>
						<li><a href="#services">services</a></li>
						<li><a href="#my_work">my work</a></li>
						<li><a href="#contact_me">contact me</a></li>
					</ul>
				</div>
			</div>
		</div><!-- header end -->
        <section id="home">
		<div class="full_wrapper full_banner">
			<div class="main_wrapper main_banner">
				<h3>Lorem ipsum dolor sit iusmod tempor</h3>
				<h1>incididunt bore</h1>
				<p>Orem ipsum dolor sit amet, consectetur</p>
				<img src="img/banner_angle.png" alt="Banner Angle" /><br />
				<a href="#">KNOW MORE</a>
			</div>
		</div>
        </section><!-- banner end -->
        <section id = "about">
		<div class="full_wrapper full_about">
			<div class="main_wrapper main_about">
				<h2>ABOUT ME</h2>
				<img src="img/about_border.png" alt="Border" />
				<p class="para">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, <span>quis nostrud exercitation ullamco</span> laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor.</p>
				<div class="icons_part">
					<div class="icons">
						<i class="fa fa-lightbulb-o" aria-hidden="true"></i>
						<h3>Title goes here</h3>
						<p>onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
					</div>
					<div class="icons">
						<i class="fa fa-pencil" aria-hidden="true"></i>
						<h3>Title goes here</h3>
						<p>onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
					</div>
					<div class="icons">
						<i class="fa fa-cog" aria-hidden="true"></i>
						<h3>Title goes here</h3>
						<p>onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
					</div>
					<div class="icons">
						<i class="fa fa-laptop" aria-hidden="true"></i>
						<h3>Title goes here</h3>
						<p>onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
					</div>
				</div>
			</div>
		</div>
        </section><!-- about end -->
        <section id = "services">
		<div class="full_wrapper full_service">
			<div class="main_wrapper main_service">
				<div class="left_part">
					<h2>SERVICES</h2>
					<img class="service_border" src="img/service_border.png" alt="border" />
					<p class="service_p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do tempor incididunt ut labore et dolore magna aliqua.</p>
					<div class="service_items">
						<div class="item">
							<img src="img/service1.png" alt="service" />
							<h3>Title goes here</h3>
							<p>Onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
						</div>
						<div class="item">
							<img src="img/service2.png" alt="service" />
							<h3>Title goes here</h3>
							<p>Onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
						</div>
						<div class="clr"></div>
						<div class="item">
							<img src="img/service3.png" alt="service" />
							<h3>Title goes here</h3>
							<p>Onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
						</div>
						<div class="item">
							<img src="img/service4.png" alt="service" />
							<h3>Title goes here</h3>
							<p>Onsectetur adipisicing elit, sedo eiusmod tempor incidi et dolorerserss eerhfre mag.</p>
						</div>
					</div>
				</div>
				<div class="right_part"></div>
			</div>
		</div>
    </section><!-- service end -->
    <section id = "my_work">
		<div class="full_wrapper full_work">
			<div class="main_wrapper main_work">
				<h2>MY WORK</h2>
				<img class="service_border" src="img/service_border.png" alt="border" />
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div>
		<div class="full_wrapper full_portfolio">
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work1.jpg" alt="Portfolio" />
			</div>
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work2.jpg" alt="Portfolio" />
			</div>
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work3.jpg" alt="Portfolio" />
			</div>
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work4.jpg" alt="Portfolio" />
			</div>
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work5.jpg" alt="Portfolio" />
			</div>
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work6.jpg" alt="Portfolio" />
			</div>
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work7.jpg" alt="Portfolio" />
			</div>
			<div class="works">
				<div class="overlay">
					<h4>Title goes here</h4>
					<p>onsectetur adipisicing elit, sedo eiusmod tempor incidiet dolorerserss eerhfre mag.</p>
				</div>
				<img src="img/work8.jpg" alt="Portfolio" />
			</div>
		</div>
    </section><!-- work end -->
    <section id = "contact_me">
		<div class="full_wrapper full_contact_up">
			<div class="main_wrapper main_contact_up">
				<h2>CONTACT ME</h2>
				<img class="service_border" src="img/service_border.png" alt="border" />
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			</div>
		</div>
		<div class="full_wrapper full_contact_down">
			<div class="main_wrapper main_contact_down">
				<form action="contact.php" method="POST">
					<input type="text" class="name" id="name" name="name" placeholder="NAME" /><br />
					<input type="email" class="mail" id="mail" name="mail" placeholder="EMAIL ADDRESS" /><br />
					<textarea name="message" id="message" class="message" placeholder="MESSAGE" cols="92" rows="2"></textarea><br />
					<input type="submit" class="submit" id="submit" name="submit" value="SUBMIT"/>
				</form>
			</div>
		</div>
    </section><!-- contact end -->
		<div class="full_wrapper full_footer">
			<div class="main_wrapper main_footer">
				<p>&copy; MYFOLIO</p>
			</div>
		</div><!-- footer end -->
	</body>
</html>
