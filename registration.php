<?php
    session_start();
    require_once 'database.php';

    if(isset($_SESSION['registration_first'])){
        echo $_SESSION['registration_first'];
        unset($_SESSION['registration_first']);
    }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Registration</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1>Registration</h1>

                    <form style="float:right" action="login.php" >
                      <button  class="btn btn-info" type="submit" name = "registration" value = "one">LOGIN</button>
                    </form>

                    <form action="registration_check.php" method="post">
                        <div class="form-group">
                            <label for="">Username</label>
                            <input class="form-control" type="text" name="username">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input class="form-control" type="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input class="form-control" type="password" name="password">
                        </div>
                        <div class="form-group">
                            <button class="form-control btn btn-success" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
